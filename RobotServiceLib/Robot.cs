﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RobotServiceLib
{
    [DataContract]
    public class Robot
    {
        [DataMember]
        public string Encoder { get; set; }

        [DataMember]
        public string Battery { get; set; }
    }
}
