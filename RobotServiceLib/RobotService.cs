﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace RobotServiceLib
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RobotService" in both code and config file together.
    public class RobotService : IRobotService
    {
        public string UpdateRobotStatus( Robot robot )
        {
            Console.WriteLine( robot.Battery + " _ " + robot.Encoder );
            return "OK";
        }
    }
}
