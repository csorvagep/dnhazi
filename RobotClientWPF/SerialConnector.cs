﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RobotClientWPF
{
    public class SerialConnector
    {
        private SerialPort port = new SerialPort();
        private const int baudRate = 115200;
        private Dictionary<string, IPeripheral> peripherals = new Dictionary<string, IPeripheral>();
        private string serialReceiveBuffer = "";

        public SerialConnector( )
        {
            port.DataReceived += port_DataReceived;
            port.NewLine = "\r\n";
        }

        void port_DataReceived( object sender, SerialDataReceivedEventArgs e )
        {
            serialReceiveBuffer += port.ReadExisting();
            while (serialReceiveBuffer.Contains( port.NewLine ))
            {
                int newLinePos = serialReceiveBuffer.IndexOf( port.NewLine );
                Process( serialReceiveBuffer.Substring( 0, newLinePos ) );
                serialReceiveBuffer = serialReceiveBuffer.Substring( newLinePos + 2 );
            }
        }

        private void Process( string input )
        {
            try
            {
                int commaPos = input.IndexOf( ',' );
                string lead = input.Substring( 0, commaPos );
                peripherals[lead].Process( input.Substring( commaPos + 1) );

            }
            catch (KeyNotFoundException e)
            {
                Trace.WriteLine(e.Message);
            }
        }

        public bool Connected { get; protected set; }

        public void Connect( string portName )
        {
            if (!Connected)
            {
                port.PortName = portName;
                port.BaudRate = baudRate;
                port.Open();
                Connected = true;
            }
        }

        public void Disconnect( )
        {
            if (Connected)
            {
                port.Close();
                Connected = false;
                serialReceiveBuffer = "";
            }
        }

        public void Add( IPeripheral periph )
        {
            peripherals.Add( periph.LeadString, periph );
            periph.connector = this;
        }

        public void Send( string str )
        {
            port.WriteLine( str );
        }
    }
}
