﻿using RobotClientWPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;

namespace RobotClientWPF.ViewModels
{
    public class MainViewModel
    {
        private BatteryViewModel batteryViewModel;
        private ConfigViewModel configViewModel;
        private EncoderViewModel encoderViewModel;

        ServiceHost robotServiceHost;

        public BatteryViewModel BatteryViewModel { get { return batteryViewModel; } }
        public ConfigViewModel ConfigViewModel { get { return configViewModel; } }
        public EncoderViewModel EncoderViewModel { get { return encoderViewModel; } }

        public void LoadInitialData( )
        {
            Battery b = new Battery() { Id = 1, ElectonicsBattery = 7.2, MotorBattery = 8.1 };
            batteryViewModel = new BatteryViewModel( b );

            Config c = new Config() { X = 20.0, Y = 2.3, Theta = 3.14159 };
            configViewModel = new ConfigViewModel( c );

            Encoder e = new Encoder() { Position = 1234, Velocity = 1.5 };
            encoderViewModel = new EncoderViewModel( e );

            robotServiceHost = new ServiceHost( typeof( RobotServiceLib.RobotService ) );
            robotServiceHost.Open();
        }

        ~MainViewModel( )
        {
            if (robotServiceHost != null)
            {
                robotServiceHost.Close();
            }
        }
    }
}
