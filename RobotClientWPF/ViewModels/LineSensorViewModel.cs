﻿using RobotClientWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotClientWPF.ViewModels
{
    public class LineSensorViewModel
    {
        private LineSensor lineSensor;
        private ObservableCollection<PhotoSensorViewModel> sensorList = new ObservableCollection<PhotoSensorViewModel>();
        public ObservableCollection<PhotoSensorViewModel> SensorList { get { return sensorList; } }

        public LineSensorViewModel( LineSensor ls )
        {
            lineSensor = ls;
            foreach (var item in ls.Sensors)
            {
                sensorList.Add( new PhotoSensorViewModel( item ) );
            }
        }
    }
}
