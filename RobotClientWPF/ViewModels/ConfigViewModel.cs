﻿using RobotClientWPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotClientWPF.ViewModels
{
    public class ConfigViewModel
    {
        Config config;

        public string X { get { return string.Format( "{0:0}m", config.X); } }
        public string Y { get { return string.Format( "{0:0}m", config.Y ); } }
        public string Theta { get { return string.Format( "{0:0}°", config.Theta / Math.PI * 180.0 ); } }

        public ConfigViewModel( Config c )
        {
            this.config = c;
        }
    }
}
