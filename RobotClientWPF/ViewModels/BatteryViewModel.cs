﻿using RobotClientWPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotClientWPF.ViewModels
{
    public class BatteryViewModel
    {
        Battery battery;

        public string EBatt { get { return string.Format("{0:0.0}V", battery.ElectonicsBattery); } }
        public string MBatt { get { return string.Format("{0:0.0}V", battery.MotorBattery); } }

        public BatteryViewModel( Battery b )
        {
            this.battery = b;
        }
    }
}
