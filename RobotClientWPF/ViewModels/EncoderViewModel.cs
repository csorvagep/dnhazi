﻿using RobotClientWPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RobotClientWPF.ViewModels
{
    public class EncoderViewModel
    {
        Encoder encoder;

        public int Position { get { return encoder.Position; } }
        public string Velocity { get { return string.Format( "{0:0.00}m/s", encoder.Velocity ); } }

        public EncoderViewModel( Encoder e )
        {
            this.encoder = e;
        }
    }
}
