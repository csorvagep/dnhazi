﻿using RobotClientWPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace RobotClientWPF.ViewModels
{
    public class PhotoSensorViewModel : INotifyPropertyChanged
    {
        PhotoSensor photoSensor;

        public bool IsAboveLimit
        {
            get
            {
                return photoSensor.Intensity > PhotoSensor.SensorLimit;
            }
        }

        public int Value
        {
            get
            {
                return photoSensor.Intensity;
            }
        }

        public PhotoSensorViewModel( PhotoSensor ps )
        {
            this.photoSensor = ps;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
