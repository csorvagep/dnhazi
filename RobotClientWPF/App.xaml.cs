﻿using RobotClientWPF.ViewModels;
using RobotClientWPF.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RobotClientWPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup( StartupEventArgs e )
        {
            MainView view = new MainView();
            MainViewModel mainViewModel = new MainViewModel();

            mainViewModel.LoadInitialData();

            view.DataContext = mainViewModel;
            view.Show();

            base.OnStartup( e );
        }
    }
}
