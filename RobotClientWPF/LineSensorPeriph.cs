﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotClientWPF
{
    class LineSensorPeriph : IPeripheral
    {
        private bool state;

        public List<int> SensorValues { get; protected set; }

        public LineSensorPeriph( )
        {
            LeadString = "L";
            SensorValues = new List<int>( 24 );
            for (int i = 0; i < 24; i++)
            {
                SensorValues.Add( 0 );
            }
        }

        public void Process( string data )
        {
            if (data.Length == 24)
            {
                byte[] values = System.Text.Encoding.UTF8.GetBytes( data.ToCharArray() );
                for (int i = 0; i < values.Length; i++)
                {
                    if (values[i] > (byte)'9')
                    {
                        SensorValues[i] = values[i] - (byte)'a' + 10;
                    }
                    else
                    {
                        SensorValues[i] = values[i] - (byte)'0';
                    }
                }
                OnPropertyChanged( "SensorValues" );
            }
        }

        public string LeadString { get; protected set; }

        public bool State
        {
            get { return state; }
            set
            {
                if (connector != null)
                    connector.Send( LeadString + "," + (value ? "1" : "0") );
                state = value;
            }
        }

        public SerialConnector connector { get; set; }

        protected void OnPropertyChanged( string name )
        {
            if (PropertyChanged != null)
            {
                PropertyChanged( this, new PropertyChangedEventArgs( name ) );
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
