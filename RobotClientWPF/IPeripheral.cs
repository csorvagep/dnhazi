﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotClientWPF
{
    public interface IPeripheral : INotifyPropertyChanged
    {
        void Process( string data );
        string LeadString { get; }
        bool State { get; set; }
        SerialConnector connector { get; set; }
    }
}
