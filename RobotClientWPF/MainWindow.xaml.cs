﻿using RobotClientWPF.Models;
using RobotClientWPF.ViewModels;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RobotClientWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow( )
        {
            InitializeComponent();

            string connString = @"Data Source=.\SQLEXPRESS;Initial Catalog=robot;Integrated Security=True;Connect Timeout=30; Encrypt=False; TrustServerCertificate=False; MultipleActiveResultSets=True";
            using (RobotContext ctx = new RobotContext(connString))
            {
                Battery b = ctx.Batteries.FirstOrDefault();
            }
        }
    }
}
