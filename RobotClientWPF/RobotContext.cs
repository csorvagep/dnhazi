﻿using RobotClientWPF.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace RobotClientWPF
{
    class RobotContext : DbContext
    {
        public DbSet<Battery> Batteries { get; set; }
        public DbSet<Config> Configs { get; set; }
        public DbSet<Encoder> Encoders { get; set; }
        public DbSet<MotorController> MotorControllers { get; set; }

        public RobotContext( string connStr )
            : base( connStr )
        {

        }

        public RobotContext( )
        {

        }
    }
}
