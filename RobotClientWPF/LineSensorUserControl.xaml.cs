﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RobotClientWPF
{
    /// <summary>
    /// Interaction logic for Graph.xaml
    /// </summary>
    //[System.ComponentModel.DefaultBindingProperty("SensorValues")]
    public partial class LineSensorUserControl : UserControl
    {
        private List<int> sensorValues;
        public List<int> SensorValues
        {
            get
            {
                return sensorValues;
            }
            set
            {
                sensorValues = value;
                for (int i = 0; i < sensorValues.Count; i++)
                {
                    if (sensorValues[i] < 2)
                    {
                        ((Rectangle)stackPanel.Children[i]).Fill = Brushes.White;
                    }
                    else
                    {
                        ((Rectangle)stackPanel.Children[i]).Fill = Brushes.Blue;
                    }
                }
            }
        }

        public static readonly DependencyProperty SensorValuesProperty =
            DependencyProperty.Register(
            "SensorValues",
            typeof( List<int> ),
            typeof( LineSensorUserControl ),
            new PropertyMetadata( null ) );

        public LineSensorUserControl( )
        {
            InitializeComponent();
            stackPanel.DataContext = this;
        }
    }
}
