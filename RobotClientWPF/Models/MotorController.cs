﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotClientWPF.Models
{
    public class MotorController
    {
        public int Id { get; set; }
        public double Error { get; set; }
        public double U1 { get; set; }
        public double U2 { get; set; }
        public double U { get; set; }
    }
}
