﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotClientWPF.Models
{
    public class LineSensor
    {
        private List<PhotoSensor> sensors = new List<PhotoSensor>();
        public List<PhotoSensor> Sensors { get { return sensors; } }

        public LineSensor( )
        {
            for (int i = 0; i < 24; i++)
            {
                sensors.Add( new PhotoSensor { Intensity = i } );
            }
        }
    }
}
