﻿using RobotClientWPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotClientWPF.Models
{
    public class PhotoSensor
    {
        public const int SensorLimit = 7;

        public int Intensity { get; set; }
    }
}
