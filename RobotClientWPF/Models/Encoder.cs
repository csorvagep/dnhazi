﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotClientWPF.Models
{
    public class Encoder
    {
        public int Id { get; set; }
        public int Position { get; set; }
        public double Velocity { get; set; }
    }
}
