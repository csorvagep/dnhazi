﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotClientWPF.Models
{
    public class Battery
    {
        public int Id { get; set; }
        public double ElectonicsBattery { get; set; }
        public double MotorBattery { get; set; }
    }
}
