﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotClientWPF.Models
{
    public class Config
    {
        public int Id { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double Theta { get; set; }
    }
}
