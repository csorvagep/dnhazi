﻿using RobotTestClient.RobotServiceReference;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RobotTestClient
{
    public partial class Form1 : Form
    {
        public Form1( )
        {
            InitializeComponent();
        }

        private void button1_Click( object sender, EventArgs e )
        {
            Robot robot = new Robot() { Battery = "B,7500,8300", Encoder = "E,5677,3.2" };
            using (RobotServiceClient client = new RobotServiceClient())
            {
                MessageBox.Show(client.UpdateRobotStatus( robot ));
            }
        }
    }
}
